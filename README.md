# README #

#### Reak is a breakout clone made with a few modifications and is currently powered by the free and open-source Godot game engine. ####

### Current features ###

* The most basic gameplay of breakout!
* Life system
* Score system
* [View source code.](https://bitbucket.org/OmaGash/reak/src)

### Currently developing features ###

* ~~Main Menu~~
* ~~Game Over dialog~~
* Level system

### Planned features ###

* Refining of graphics
* Powerups
* M0ar L3v3ls!!!

### Suggestions? Queries? ###

* [Francis](mailto://francislaxasantos@gmail.com)