extends Node

#To-Do's:
#/implement play button
#make play button prettier
#/implement exit button
#make exit button look prettier
onready var play = find_node('play')
onready var exit = find_node('exit') 
onready var spam = find_node('spam')

var rect
var screen

func _ready():
	set_process_input(false)
	rect = get_viewport_rect()
	screen = rect.size
	play.set_pos(screen/Vector2(2, 1.4))
	exit.set_pos(screen/Vector2(2, 1.2))
	pass


func playButtonPressed():
	print('play')
	get_tree().change_scene('res://scenes/game.tscn')
	


func playPressed():
	pass # replace with function body

func _input(event):
	if event.is_action_pressed('debug'):
		get_node('background/debug').show()

func _on_spam_toggled( pressed ):
	pass # replace with function body
