extends KinematicBody2D

var mouse
var screen
var ht
#To use this properly, make sure that there are at least 1  ball inside the game
export (bool) var unliBalls = false
#This prevents multiple balls from being spawned by the player
export (bool) var ballsPam  = false
export (bool) var autoAim   = false
export (int)  var aiLimit     = 5
var ball
var ballScene = preload('res://scenes/ball.tscn')
onready var game = get_node('/root/game')
onready var ballContainer = get_node('/root/game/ballsContainer')
var lifeLeft

func _ready():
	set_fixed_process(true)
	set_process_input(true)
	screen = get_viewport()
	ht = get_viewport_rect().size.height
	ball = get_node("../ball")


func _fixed_process(delta):
	lifeLeft = game.life
	if !autoAim:
		mouse = screen.get_mouse_pos()
		set_pos(Vector2(mouse.x, get_pos().y))

func _input(event):
	if event.type == InputEvent.MOUSE_BUTTON and event.is_pressed() and (unliBalls or lifeLeft > 0 ):
		if !ballsPam and ballContainer.get_child_count() == 0:
			var bol = ballScene.instance()
			bol.set_pos(get_pos()-Vector2(0,16))
			game.get_node('ballsContainer').add_child(bol)
			get_node("/root/game").life -= 1
		elif ballsPam:
			var bol = ballScene.instance()
			bol.set_pos(get_pos()-Vector2(0,16))
			game.get_node('ballsContainer').add_child(bol)
			get_node("/root/game").life -= 1
