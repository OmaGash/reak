extends RigidBody2D

export var speedUp = 4

const MAX_SPEED = 400
var heig
onready var player = get_node("/root/game/player")
var hitPos
var hitCounter = 0
onready var game = get_node('/root/game')



func _ready():
	randomize()
	set_fixed_process(true)
	heig = get_viewport_rect().size.height
	hitPos = rand_range(-32,32)



func _fixed_process(delta):
	
	var body = get_colliding_bodies()
	#Triggers when the ball gets off to oblivion
	if get_pos().y > heig:
		queue_free()
		#Gets called when life is == 0 and ball gets lost, in other words, gameover due to life == -1
		if get_node('/root/game').life <= 0 and game.get_node('ballsContainer').get_child_count() <= 1:
			get_node('/root/game').endHandler()
	
	if player.autoAim:
		player.set_pos(Vector2(get_pos().x-hitPos, player.get_pos().y))
	
	#This makes sure that the ball won't loop the ball when it hits nothing
	while hitCounter >= get_node('/root/game/player').aiLimit:
		randomize()
		hitPos = rand_range(-32,32)
		hitCounter = 0
	
	for collider in body:
		if collider.is_in_group("brick"):
			get_node('/root/game').score += get_node('/root/game').scoreValue
			collider.queue_free()
		
		if collider.get_name() == 'player':
			var speed = get_linear_velocity().length()
			var dir   = get_pos() - collider.get_node('Position2D').get_global_pos()
			var vel   = dir.normalized() * min(speed+speedUp, MAX_SPEED)
			set_linear_velocity(vel)
			hitCounter += 1
			print(str(speed))

