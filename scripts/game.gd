extends Node

var levels = [  preload('res://levels/level_000.tscn'),
				preload('res://levels/level_001.tscn')
			]
var currLevel = 0
var nextLevel = 0

export var resetGameOver = false
export (float) var scoreValue = 1
var bricks
onready var brick = get_node('brick')
var heig
var score = 0 setget scoreSet
var life  = 3 setget lifeSet
var level = 0 setget levelSet
onready var scoreKeeper = get_node('score')
onready var lifeKeeper  = get_node('life')
onready var levelKeeper = get_node('level')
onready var player      = get_node('player')
onready var debug       = get_node('debug')

func _ready():
	set_process(true)
	set_process_input(true)
	heig = get_node("player").ht
	lifeKeeper.set_text('Lives: ' + str(life))
	
	pass

func _process(delta):
	if currLevel == nextLevel:
		brick.add_child(levels[currLevel].instance())
		currLevel = currLevel
		nextLevel = currLevel + 1
	
	bricks = brick.get_child(0).get_child_count()
	if bricks == 0 :
		endHandler()
	if debug.is_visible():
		get_tree().set_pause(true)

func _input(event):
	if event.is_action_pressed("debug"):
		debug.show()

func debugger(index):
	if debug.get_node(index).is_toggled():
		player.ballsPam = true
	pass

#Next Level Handler
func nextLevel():
	pass

#Gets called either when Life is 0 or Bricks are 0
func endHandler():
	print('triggered')
	#Resets the game
	if  resetGameOver and (bricks <= 0 or life <= 0):
		get_tree().change_scene("res://scenes/game.tscn")
	#Do something on level ending (all bricks destroyed)
	if !resetGameOver and bricks <= 0:
		
		#nextLevel(nextLevel)
		get_tree().quit()
	if !resetGameOver and life <= 0:
		get_node("gameover").popup()

func levelSet(newLevel):
	level = newLevel
	levelKeeper.set_text("Level: "    + str(level))

func scoreSet(newScore):
	score = newScore
	scoreKeeper.set_text("Score: "    + str(score))

func lifeSet(newLife):
	life = newLife
	
	
	if life == 1:
		lifeKeeper.set_text('Life: '  + str(life))
	else:
		lifeKeeper.set_text('Lives: ' + str(life))

#Gameover dialog
func _on_gameover_confirmed():
	get_tree().change_scene('res://scenes/menu.tscn')
