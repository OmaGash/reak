extends WindowDialog

onready var player = get_tree().get_root().get_child(0).find_node('player')
onready var game   = get_tree().get_root().get_child(0)

func _ready():
	print(player.get_name())
	pass


func _on_debug_hide():
	print('triggered')
	get_tree().set_pause(false)


func _on_spam_toggled( pressed ):
	player.ballsPam = true




func _on_rset_toggled( pressed ):
	game.resetGameOver = true


func _on_unli_toggled( pressed ):
	player.unliBalls = true


func _on_abot_toggled( pressed ):
	player.autoAim = true
